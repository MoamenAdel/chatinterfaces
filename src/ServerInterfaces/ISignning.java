/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerInterfaces;

/**
 *
 * @author Sara Selim
 */
public interface ISignning {

    /**
     *
     * @param user
     * @return
     */
    public boolean signUp(IUser user);

    /**
     *
     * @param id
     * @return
     */
    public boolean signOut(int id);

    /**
     *
     * @param id
     * @return
     */
    public boolean signIn(int id);
}
