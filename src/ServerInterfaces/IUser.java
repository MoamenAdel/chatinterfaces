/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerInterfaces;

import ServerInterfaces.Status;

/**
 *
 * @author Sara Selim
 */
public interface IUser {

    /**
     *
     * @return id
     */
    public int getId();

    /**
     *
     * @return last status
     */
    public Status getLastStatus();

    /**
     *
     * @return gender
     */
    public String getGender();

    /**
     *
     * @return name
     */
    public String getName();

    /**
     *
     * @return
     */
    public String getEmail();

    /**
     *
     * @return country
     */
    public String getCountry();

    /**
     *
     * @return type
     */
    public String getType();

    /**
     *
     * @param id
     */
    public void setId(int id);

    /**
     *
     * @param name
     */
    public void setName(String name);

    /**
     *
     * @param type
     */
    public void setType(String type);

    /**
     *
     * @param country
     */
    public void setCountry(String country);

    /**
     *
     * @param email
     */
    public void setEmail(String email);

    /**
     *
     * @param gender
     */
    public void setGender(String gender);

    /**
     *
     * @param status
     */
    public void setLastStatus(Status status);
}
